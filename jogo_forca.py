import random
import os

HANGMANPICS = ['''

  +---+
  |   |
      |
      |
      |
      |
=========''', '''

  +---+
  |   |
  O   |
      |
      |
      |
=========''', '''

  +---+
  |   |
  O   |
  |   |
      |
      |
=========''', '''

  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''

  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========''', '''

  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========''', '''

  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========''']

palavras = 'macaco baleia gorila pato humano cavalo galo galinha boi sapo carro bicicleta hospital vassoura'.split()

def __hangman__(value):
	global HANGMANPICS
	if value == 0:
		print(HANGMANPICS[0])
	elif value == 1:
		print(HANGMANPICS[1])
	elif value == 2:
		print(HANGMANPICS[2])
	elif value == 3:
		print(HANGMANPICS[3])
	elif value == 4:
		print(HANGMANPICS[4])
	elif value == 5:
		print(HANGMANPICS[5])
	elif value == 6:
		print(HANGMANPICS[6])

def __init_hangman__():
	global letraCerta
	global letraErrada
	global espacos
	global palavraSecreta
	letraCerta = 0
	letraErrada = 0
	espacos = '_'
	palavraSecreta = randomPalavra(palavras)
	espacos = espacos * len(palavraSecreta)

def __print_secretWord__(word):
	for i in range(len(word)):
		print(word[i], end = ' ')
	print()

def is_number(s):
	try:
		float(s)
		return True
	except ValueError:
		return False

def randomPalavra(listaPalavras):
		palavraIndex = random.randint(0, len(listaPalavras) - 1)
		return listaPalavras[palavraIndex]

def tentativa():
	print('Entre com uma letra.\n')
	letra = input().lower()
	while is_number(letra) == True:
		print('Entre somente com UMA LETRA.\n')
		letra = input().lower()
	while len(letra) != 1:
		print('Entre somente com UMA LETRA.\n')
		letra = input().lower()

	return letra 

def verificacao():
	global letraErrada
	global letraCerta
	global espacos

	vetorLetra = []

	while letraErrada < 6 and espacos != palavraSecreta:
		result = False
		letra = tentativa()
		if letra in vetorLetra:
			print('Voce ja entrou com essa letra. Entre com outra.\n')
		else:
			vetorLetra.append(letra)

		for i in range(len(palavraSecreta)):
			if letra in palavraSecreta[i]:
				result = True
				espacos = espacos[:i] + letra + espacos[i+1:]
		__print_secretWord__(espacos)
		if(result == False):
			letraErrada = letraErrada + 1
		__hangman__(letraErrada)
	if(letraErrada == 6):
		print('Voce perdeu! A palavra correta era: ' + palavraSecreta)
		__hangman__(letraErrada)
		return
	print('Parabens! Voce acertou a palavra!\n')

letraCerta = 0
letraErrada = 0
espacos = '_'
palavraSecreta = randomPalavra(palavras)
espacos = espacos * len(palavraSecreta)

jogar_novamente = 'sim'
print('\n~Bem-vindo ao Forcastein!~\nTente adivinhar a palavra em que estou pensando.\nVoce pode errar, no maximo, 6 vezes!\n')
while jogar_novamente == 'sim' or jogar_novamente == 's':
	verificacao()
	print('Voce deseja jogar novamente? (Sim ou nao)\n')
	jogar_novamente = input().lower()
	__init_hangman__()
	os.system('cls') #on windows